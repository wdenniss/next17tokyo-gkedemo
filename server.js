var express = require('express');
var app = express();
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
process.on('SIGINT', function() { console.log('exiting'); process.exit(); });

// dynamic index route
app.get('/', function(req, res) { res.render('index'); });

// static assets
app.use('/', express.static(__dirname + '/public'));

// listen for HTTP requests on port 3000
app.listen(3000, function() { console.log('listening'); });

